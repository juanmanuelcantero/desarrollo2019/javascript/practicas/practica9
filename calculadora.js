window.addEventListener("load",function(e){
	let botonesNumericos=document.querySelectorAll(".botones");
	let botonesOperaciones=document.querySelectorAll(".operaciones");
	let salida=document.querySelector(".display");
	let numerosAnterior=0;
	let operacionActual="";
	let operacionPendiente="";

	botonesNumericos.forEach(function(v)  {
	    v.addEventListener("click",function(e) {
	    	if(operacionActual!=""){
	    		operacionPendiente=operacionActual;
	    		operacionActual="";
	    		salida.innerHTML=e.target.innerHTML;
	    	}else{
	    		salida.innerHTML+=e.target.innerHTML;	
	    	}
	    	
	      
	    });
	});

	botonesOperaciones.forEach(function(item) {
	  item.addEventListener("click",function(e)  {
	  	if(operacionActual!=""){
	  		operacionActual=e.target.innerHTML;
	  	}else if(operacionPendiente==""){
	    	numerosAnterior=parseInt(salida.innerHTML);
	    	operacionActual=e.target.innerHTML;
	    }else{
	    	//eval("numerosAnterior=numerosAnterior"+ operacionPendiente + "parseInt(salida.innerHTML)");
	    	switch (operacionPendiente) {
	    		case "+":
	    			numerosAnterior=numerosAnterior+parseInt(salida.innerHTML);
	    			break;
	    		case "-":
	    			numerosAnterior=numerosAnterior-parseInt(salida.innerHTML);
	    			break;
	    		case "*":
	    			numerosAnterior=numerosAnterior*parseInt(salida.innerHTML);
	    			break;
	    		case "/":
	    			numerosAnterior=numerosAnterior/parseInt(salida.innerHTML);
	    			break;
	    	}
	    	operacionActual=e.target.innerHTML;
	    	salida.innerHTML=numerosAnterior;
	    }
	    
	  });
	})
	
});